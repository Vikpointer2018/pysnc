import numpy as np

src = np.genfromtxt('src01.csv', dtype='int32', delimiter=r';')
# src = np.genfromtxt('src01.csv', dtype='int32', delimiter=r';', names=True)
print('Загружаем созданную в Excel таблицу в numpy:')
print(src)

shap = src.shape

axis1 = shap[0]
axis2 = shap[1]
print('Загруженная матрица имеет размеры ', axis1, 'x', axis2)

minaxis = min(axis1, axis2)
print('Создадим единичную матрицу размерности  ', minaxis, 'x', minaxis, ' :')
ones = np.zeros((minaxis, minaxis))
for ij in range(minaxis):
    ones[ij][ij] = 1
print(ones)

print('Перемножим полученные матрицы и сохраним врезультат в файле :')
result = np.dot(src, ones)
print(result)
np.savetxt("result.csv", result, fmt='%i', delimiter=";")


