from tkinter import *
import numpy as np
import pandas as pd
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from scipy.linalg import solve


def show_head(parent):  # Заголовок
    fr_head = Frame(parent, width=900, height=40, bg="#eeeeee", )
    Label(fr_head, text="Расчет баланса продуктов", font=('', 16)).pack()
    fr_head.pack()


def show_body(parent):  # отрисовка блока данных
    global cells
    global needs
    global prods_init  # также понадобится при отрисовке диаграммы
    global columns
    cells_init = np.array(
        [
            [1, 10, 1, 2, 2],
            [9, 1, 0, 1, 1],
            [2, 2, 5, 1, 2],
            [1, 1, 1, 2, 13],
            [1, 1, 1, 9, 2]
        ]
    )
    needs_init = np.array([170, 180, 140, 180, 350])

    fr_body = Frame(parent, width=900, height=300, bg="#f0f0f0", padx=10, pady=10)
    i = 0
    for nam in ['Продукт'] + columns:
        add_entry(fr_body, i, 0, nam)
        i += 1

    for y in range(len(cells_init)):
        row_cells = []
        add_entry(fr_body, 0, y + 1, prods_init[y])  # стобец продуктов
        for x in range(1, len(cells_init[0]) + 1):
            cell = add_entry(fr_body, x, y+1, cells_init[y][x-1])
            row_cells.append(cell)
        cells.append(row_cells)

    # добавим строку параметров
    add_entry(fr_body, 0, len(cells_init)+1, 'Норма')
    for x in range(len(cells_init[0])):
        cell = add_entry(fr_body, x + 1, len(cells_init)+1, needs_init[x])
        needs.append(cell)
    fr_body.pack()
    return fr_body


def add_entry(parent, rx, ry, val):  # отрисовка содержимого ячейки
    fr_item = Frame(parent, padx=10, pady=10)    
    if ry < 1 or rx < 1:  # заголовки сверху и названия продуктов слева
        ent_item = Label(fr_item, width=9,  text=val, font=("", 13, "bold"))
    else:
        ent_item = Entry(fr_item, width=9, font=("", 13))
        ent_item.insert(0, val)
    ent_item.pack()
    fr_item.grid(row=ry, column=rx)
    
    return ent_item


def show_footer(parent):  # блок кнопок, и потом сюда же отрисуется диаграмма

    fr_foot = Frame(parent, width=900, height=260, bg="#fff", )
    btn_go = Button(fr_foot, width=25, height=1, bg="#00ffff", text="Рассчитать", command=calc, font=('', 15))
    btn_savedata = Button(fr_foot, width=25, height=1, bg="#00ffff", text="Сохранить данные", command=savedata, font=('', 15))
    btn_saveimg = Button(fr_foot, width=25, bg="#00ffff", text="Сохранить диаграмму", command=saveimg, font=('', 15))
    btn_go.place(relx=0.6, rely=0.12)
    btn_savedata.place(relx=0.6, rely=0.42)
    btn_saveimg.place(relx=0.6, rely=0.72)
    
    fr_foot.pack()
    return fr_foot


def calc():  # расчет и отображение диаграммы
    # Сначала соберем все данные из таблицы в матрицы
    matr_a = np.zeros((5, 5))
    matr_b = np.zeros(5)
    for y in range(len(cells)):
        for x in range(len(cells[0])):
            val = cells[x][y].get()
            matr_a[y][x] = float(val)
        matr_b[y] = float(needs[y].get())
    print(matr_a)
    print(matr_b)
    x = solve(matr_a, matr_b.reshape((5, 1)))
    print(x)

    sizes = x.flatten()
    draw_pic(sizes)


def draw_pic(sizes):  # отрисовка диаграммы
    global prods_init  # список продуктов также нужен в основной таблице
    global fig  # понадобится снаружи для сохранения диаграммы
    # список разных цветов для диаграммы
    colors_tpl = ['red', 'orange', 'yellow', 'green', 'blue', 'violet', 'grey', 'pink', 'brown', 'black']
    labels = prods_init  # уповаем, что размерности совпадают
    colors = []
    explode = []
    for i in range(len(sizes)):
        colors.append(colors_tpl[i])
        explode.append(0.1)
        if sizes[i] < 0:
            sizes[i] = 0
    fig = Figure()
    ax = fig.add_subplot(111)
    ax.pie(sizes, explode=explode, labels=labels, colors=colors, autopct='%1.1f%%', shadow=True, startangle=140)

    canvas1 = FigureCanvasTkAgg(fig, master=ftr)
    canvas1.get_tk_widget().place(x=0, y=0, width=400, height=260)


def savedata():  # сохранение данных
    global cells
    global needs
    global columns
    global prods_init
    if cells is None or needs is None:
        warning_dialog("Нет данных для сохранения")  # вдруг чо
        return

    save_matrix = np.zeros((len(cells)+1, len(cells[0])))
    for y in range(len(cells)):
        for x in range(len(cells[0])):
            try:
                save_matrix[y][x] = float(cells[y][x].get())
            except ValueError:
                pass
    for x in range(len(needs)):
        try:
            save_matrix[len(cells)][x] = float(needs[x].get())
        except ValueError:
            pass

    save_df = pd.DataFrame(save_matrix, columns=columns, index=prods_init+['Норма'])
    # диалог сохранения данных
    save_dialog('vitamins.csv', '.csv', save_df)


def saveimg():  # сохранение изображения диаграммы
    global fig
    if fig is None:
        warning_dialog("Нет диаграммы для сохранения")
    else:
        save_dialog('vitamins.jpg', '.jpg', fig)


def save_dialog(fname_def, ext, data):
    def sav():
        fname = ent_file.get()
        if fname.endswith(ext):
            fname = fname.replace(ext, '')
        fname += ext

        if ext == '.jpg':
            data.savefig(fname)
        else:
            data.to_csv(fname, sep=";")
        fr_file.destroy()

    fr_file = Frame(ftr, width=400, height=20, padx=10, pady=10, borderwidth=3)
    Label(fr_file, text='Введите название файла').pack()
    ent_file = Entry(fr_file, width=15, font=("", 13))
    ent_file.insert(0, fname_def)
    ent_file.pack()
    Button(fr_file, text='Save', command=sav).pack()
    fr_file.place(relx=0.4, rely=0.2)


def warning_dialog(msg):
    def confirm():
        fr_warn.destroy()

    global ftr
    fr_warn = Frame(ftr, width=400, height=20, padx=10, pady=10, borderwidth=3, bg="red")
    Label(fr_warn, text=msg, bg='red', font=('', 13)).pack()
    Button(fr_warn, text='Ok', command=confirm).pack()
    fr_warn.place(relx=0.35, rely=0.3)


cells = []
needs = []
columns = ['Витамин А', 'Витамин В', 'Витамин С', 'Витамин D', 'Витамин E']
prods_init = ['Картофель', 'Моркофель', 'Мясо', 'Шмясо', 'Молоко']
fig = None

root = Tk()
root.title("Расчет баланса продуктов")
root.geometry("900x600")

show_head(root)
show_body(root)
ftr = show_footer(root)

root.mainloop()
