from os import path
from tkinter import Tk, Menu, filedialog
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


def show_menu(parent):
    menubar = Menu(parent)
    parent.config(menu=menubar)
    file_menu = Menu(menubar, tearoff=0)
    file_menu.add_command(label="Загрузить", command=lambda: load_data(parent))
    file_menu.add_command(label='Выход', command=quit)
    menubar.add_cascade(label="Загрузить данные для просмотра", menu=file_menu)


def load_data(parent):
    file1 = filedialog.askopenfilename(filetypes=(("CSV files", "*.csv"), ("all files", "*.*")), initialdir=path.dirname(__file__))
    print(file1)
    if file1 is None or file1 == '':
        messagebox.showinfo("Ошибка загрузки", "Файл не выбран")
        return None
    try:
        df = pd.read_csv(file1, sep=';')
    except UnicodeDecodeError:  # Если не utf-8, то возможно cp-1251
        df = pd.read_csv(file1, sep=';', encoding='cp1251')

    # Говорим, что индексами будет первый столбец
    # (Продукты, по умолчанию создаётся шапка Unnamed: 0)
    draw_graf(parent, df.set_index('Unnamed: 0'))


def draw_graf(parent, df):  # Замена значений оси витаминов
    def formatox(x, pos):
        x = int(round(x))
        if x < 0:  # костыль. индексы иногда выскакивают за границы
            x = 0
        if x > 4:
            x = 4
        return columns[x]

    def formatoy(y, pos):  # Замена обозначений оси продуктов
        y = int(round(y))
        if y < 0:  # костыль. индексы иногда выскакивают за границы
            y = 0
        if y > 5:
            y = 5
        return index[y]

    if df is None:  # вдруг пусто
        return
    # Посмотреть, чего загрузилось
    columns = df.columns
    index = df.index
    index.name = None  # Убрали добавочную шапку у столбца индексов (продуктов)
    print(df)

    # Обработать данные (заполнить содержимое случайными значениями)
    df2 = pd.DataFrame(np.random.rand(len(index), len(columns)), dtype=float)
    print(df2)

    # Нарисовать поверхность
    x = df2.columns
    y = df2.index
    X, Y = np.meshgrid(x, y)
    Z = df2

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X, Y, Z, cmap='inferno')
    ax.xaxis.set_major_formatter(FuncFormatter(formatox))  # Заменяем обозначения осей
    ax.yaxis.set_major_formatter(FuncFormatter(formatoy))  # на витамины/продукты

    canvas1 = FigureCanvasTkAgg(fig, master=parent)
    canvas1.get_tk_widget().place(x=10, y=10, width=680, height=480)


win = Tk()
win.title('Отображение графика')
win.geometry('700x500')

show_menu(win)

win.mainloop()
